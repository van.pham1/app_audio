import 'package:app_audio/api_url.dart';
import 'package:app_audio/manager/facebook_login_manager.dart';
import 'package:app_audio/model/list_category.dart';
import 'package:app_audio/model/user.dart';
import 'package:dio/dio.dart';
import 'dart:convert' as JSON;
import 'dart:io' show Platform;

import 'app_dio.dart';

class ApiWebservice {
  Future<User> signInFacebook() async {
    final result = await FacebookSigninManager().signin();
    final String token = result.accessToken.token;
    var graphResponse = await Dio().get(
        'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200)&access_token=' +
            token);
    var profile = JSON.jsonDecode(graphResponse.data);
    return await signInWithType(
        type: APIParams.facebook,
        id: profile[APIParams.id],
        name: profile[APIParams.name],
        avatar: profile[APIParams.avatar],
        email: profile[APIParams.email]);
  }

  Future<User> signInWithType(
      {String type,
      String id,
      String name,
      String avatar,
      String email}) async {
    User userModel;

    var key = APIParams.fbId;
    switch (type) {
      case 'google':
        key = APIParams.googleId;
        break;
      case 'apple':
        key = APIParams.appleId;
        break;
      case 'facebook':
        key = APIParams.fbId;
        break;
    }

    Map<String, dynamic> params = {
      APIParams.version: "1.3",
      APIParams.communication: Platform.isIOS ? "ios" : "android",
      APIParams.type_login: type,
      key: id,
      APIParams.name: name,
      APIParams.avatar: avatar,
      // APIParams.email: email,
      "api": ApiCons.login,
    };
    final customDio = await AppDio().getAppDio(false);
    final response = await customDio.getPostReponse(ApiCons.login, params);
    userModel = User.fromJson(response.data["data"]);
    return userModel;
  }


  Future<ListCategory> getListSimilarStory(int cateType, int page) async {
    ListCategory listCategory;
    Map<String, dynamic> params = {
      APIParams.version: "1.3",
      APIParams.communication: Platform.isIOS ? "ios" : "android",
      APIParams.page: page,
      APIParams.limit: 20,
      APIParams.categoryType: cateType,
    };

    try {
      final customDio = await AppDio().getAppDio(true);
      final response =
      await customDio.getReponseGetAPI(ApiCons.storyCat, params);

      listCategory = ListCategory.fromJson(response.data);
    } on DioError catch (error) {
      print("ApiWebserver: signinFacebook error: ");
      print(error.response.statusCode);
      throw (error.response.statusCode);
    }
    return listCategory;
  }

  Future<ListCategory> getListCate() async {
    ListCategory listCategory;
    Map<String, dynamic> params = {
      APIParams.version: "1.3",
      APIParams.communication: Platform.isIOS ? "ios" : "android",
      APIParams.page: 1,
      APIParams.limit: 20,
      APIParams.pageCategory: 1,
      APIParams.limitCategory: 20,
    };
    try {
      final customDio = await AppDio().getAppDio(true);
      final response =
          await customDio.getReponseGetAPI(ApiCons.storyCat, params);
      listCategory = ListCategory.fromJson(response.data);
    } on DioError catch (error) {
      print(error.response.statusCode);
      throw (error.response.statusCode);
    }
    return listCategory;
  }

  Future<ListCategory> getListStory() async {
    ListCategory listCategory;
    Map<String, dynamic> params = {
      APIParams.version: "1.3",
      APIParams.communication: Platform.isIOS ? "ios" : "android",
      APIParams.page: 1,
      APIParams.limit: 20,
    };

    try {
      final customDio = await AppDio().getAppDio(true);
      final response =
      await customDio.getReponseGetAPI(ApiCons.getListStoryUp, params);

      listCategory = ListCategory.fromJson(response.data);
    } on DioError catch (error) {
      print("ApiWebserver: signinFacebook error: ");
      print(error.response.statusCode);
      throw (error.response.statusCode);
    }
    return listCategory;
  }
}
