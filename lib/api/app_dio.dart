import 'package:app_audio/api_url.dart';
import 'package:app_audio/manager/shared_preference.dart';
import 'package:app_audio/model/user.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';

import '../constants.dart';

class AppDio with DioMixin implements Dio {
  Future<AppDio> getAppDio(bool hasToken) async {
    BaseOptions options = BaseOptions(
      baseUrl: ApiCons.baseURL,
      contentType: 'application/json',
      connectTimeout: 30000,
      sendTimeout: 30000,
      receiveTimeout: 30000,
    );
    if (hasToken) {
      options.headers = await getToken();
    }
    this.options = options;
    interceptors..add(DioCacheManager(CacheConfig(baseUrl: ApiCons.baseURL))
        .interceptor as Interceptor)..add(LogInterceptor(requestBody: false));
    httpClientAdapter = DefaultHttpClientAdapter();
    return this;
  }

  Future<Response<dynamic>> getPostReponse(String apiPath,
      Map<String, dynamic> param) async {
    try {
      final reponse = await this.post(apiPath, data: param);
      print("------- Response api: $apiPath data: $reponse -------");
      return reponse;
    } on DioError catch (error) {
      print(
          "------- Request API: $apiPath - Error code: ${error.response
              .statusCode}");
      throw (error.response.statusCode);
    }
  }

  Future<Response<dynamic>> getReponseGetAPI(
      String apiPath, Map<String, dynamic> param) async {
    try {
      final reponse = await this.get("/" + apiPath, queryParameters: param);
      print("------- Response api: $apiPath data: $reponse -------");
      return reponse;
    } on DioError catch (error) {
      print(
          "------- Request API: $apiPath - Error code: ${error.response.statusCode}");
      throw (error.response.statusCode);
    }
  }

  Future<Map<String, dynamic>> getToken() async {
    User userLogin = User.fromJson(
        await SharedPreference.shared.read(SharedPrefCons.userLogin));
    print("------Token: ${userLogin.token}");
    Map<String, dynamic> params = {
      "Authorization": "Bearer " + userLogin.token,
    };
    return params;
  }

}

