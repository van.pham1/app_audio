import 'package:app_audio/model/list_category.dart';
import 'package:flutter/material.dart';


class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class AppConstant {
  static AppConstant shared = AppConstant();
  ListCategory listCate;
}

class SharedPrefCons {
  static String userLogin = "user_login";
}