import 'dart:async';
import 'package:app_audio/constants.dart';
import 'package:app_audio/widget/home_page.dart';
import 'package:app_audio/widget/login_page.dart';
import 'package:app_audio/widget/root_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final _splashDelay = 2;
  var _currentTime = 0;
  Timer timer;

  void navigationPage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => Scaffold(body: RootPage())));
  }

  @override
  void initState() {
    super.initState();
    _loadWidget();
  }

  _loadWidget() async {
    var _duration = Duration(seconds: 1);
    timer = Timer.periodic(_duration, (timer) {
      setState(() {
        _currentTime += 1;
        if (_currentTime > _splashDelay) {
          navigationPage();
        }
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      constraints: BoxConstraints.expand(),
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage('assets/images/splash_image.png'),
        fit: BoxFit.cover,
      )),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(image: AssetImage('assets/images/cloud_image.png')),
            Container(
              margin: EdgeInsets.only(top: 6),
              padding: EdgeInsets.only(left: 20, right: 20),
              width: 200,
              child: FAProgressBar(
                size: 4,
                borderRadius: 0,
                border: Border.all(
                  color: Colors.black,
                  width: 0.5,
                ),
                backgroundColor: Colors.white,
                currentValue: (_currentTime * 100 / _splashDelay).round(),
                changeProgressColor: Color.fromRGBO(0, 0, 0, 0.8),
                progressColor: HexColor('#1E562A'),
                direction: Axis.horizontal,
              ),
            ),
            Container(
              child: Text('Truyện Cổ Tích Audio',
                  style: TextStyle(fontFamily: 'Pacifico', fontSize: 24)),
            ),
          ],
        ),
      ),
    );
  }
}
