import 'dart:async';

import 'package:app_audio/constants.dart';
import 'package:app_audio/model/category_model.dart';
import 'package:app_audio/model/story_model.dart';
import 'package:app_audio/provider/home_provider.dart';
import 'package:app_audio/widget/bottom_audio_home.dart';
import 'package:app_audio/widget/category_cell.dart';
import 'package:app_audio/widget/left_menu_page.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import 'package:flutter/foundation.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import 'home_list_story_cell.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentPageValue = 0;
  int previousPageValue = 0;
  Timer timer;
  PageController pageController;
  List<String> listBanner = [
    'assets/images/banner1.png',
    'assets/images/banner2.png',
    'assets/images/banner3.png',
    'assets/images/banner4.png'
  ];
  // List<String> listCategory = [
  //   'Truyện cổ tích Việt Nam',
  //   'Truyện cổ tích thế giới',
  //   'Truyện cổ tích Việt Nam',
  //   'Truyện cổ tích thế giới'
  // ];
  final positionNotifier = ValueNotifier<int>(0);
  @override
  void initState() {
    super.initState();
    pageController = PageController();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      Provider.of<HomeProvider>(context, listen: false)
          .getData()
          .then((value) => {print('done')});
    });
    var _duration = Duration(seconds: 3);
    timer = Timer.periodic(_duration, (timer) {
      if (listBanner != null && pageController.hasClients) {
        currentPageValue += 1;
        if (currentPageValue == listBanner.length) {
          currentPageValue = 0;
        }
        pageController.animateToPage(
          currentPageValue,
          duration: Duration(milliseconds: 350),
          curve: Curves.easeIn,
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context);
    print('======${homeProvider.listCategory}');
    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 30,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              border: Border.all(color: HexColor("#1E562A"))),
          child: Container(
              child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: const EdgeInsets.all(0),
                  height: 18.0,
                  width: 22.0,
                  child: Image.asset(
                    'assets/images/search.png',
                    fit: BoxFit.fitHeight,
                  ),
                ),
                Text(
                  'Tìm Kiếm Truyện',
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
              ],
            ),
          )),
        ),
        backgroundColor: Colors.white,
        iconTheme: new IconThemeData(color: Colors.black),
        elevation: 0,
        actions: [
          Container(
            padding:
                const EdgeInsets.only(top: 2, bottom: 2, left: 4, right: 19),
            height: 30.0,
            width: 49.0,
            child: Image.asset(
              'assets/images/icon_vip.png',
              fit: BoxFit.fitWidth,
            ),
          )
        ],
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              iconSize: 30,
              padding: new EdgeInsets.all(0),
              icon: Image.asset(
                'assets/images/icon_left_menu.png',
                fit: BoxFit.fitHeight,
              ),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
      ),
      bottomNavigationBar: BottomAudioHome(),
      drawer: NavDrawer(),
      body: Container(
        color: Colors.white,
        child: ListView(
          children: [
            headerWidget(listBanner),
            ValueListenableBuilder(
              valueListenable: positionNotifier,
              builder: (_, value, __) => Container(
                height: 8,
                child: _buildPageIndicator(),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            (homeProvider.listStory != null)
                ? ListView.builder(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemCount: (homeProvider.listStory.data.length > 3) ? 3 : homeProvider.listStory.data.length ,
                    itemBuilder: (BuildContext context, int index) {
                      return HomeListStory(
                        section: index,
                        listCate: homeProvider.listStory.data,
                      );
                    })
                : Container(),
            Container(
              margin: EdgeInsets.only(left: 20,right: 20,bottom: 12),
              child: Text(
                'Thể loại truyện',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, ),
              ),
            ),
            (homeProvider.listCategory != null)
             ? ListView.builder(
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              itemCount: homeProvider.listCategory.data.length,
                itemBuilder: (BuildContext context, int index) {
              return CategoryCell(listCate: homeProvider.listCategory.data[index].title,);
            }) : Container(),
          ],
        ),
      ),
    );
  }

  headerWidget(List<String> imgList) {
    return Container(
        height: 240,
        child: Center(
            child: PageView.builder(
                controller: pageController,
                scrollDirection: Axis.horizontal,
                itemCount: imgList.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 14),
                    child: Image.asset(
                      imgList[index],
                      fit: BoxFit.fitWidth,
                    ),
                  );
                },
                onPageChanged: (int page) {
                  currentPageValue = page;
                  positionNotifier.value = currentPageValue;
                })));
  }

  _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < listBanner.length; i++) {
      list.add(i == currentPageValue ? _indicator(true) : _indicator(false));
    }
    return Container(
      height: 10,
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: list,
        ),
      ),
    );
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 3.0),
      height: 6.0,
      width: 6.0,
      decoration: BoxDecoration(
        color: isActive ? HexColor('#AB3611') : HexColor('#F1E2CF'),
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }
}
