import 'package:app_audio/constants.dart';
import 'package:app_audio/provider/audio_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../api_url.dart';

class BottomAudioHome extends StatefulWidget {
  @override
  _BottomAudioHomeState createState() => _BottomAudioHomeState();
}

class _BottomAudioHomeState extends State<BottomAudioHome> {
  @override
  Widget build(BuildContext context) {
    final audioProvider = Provider.of<AudioProvider>(context);
    return Container(
        height:
            (audioProvider.storyModel != null && audioProvider.isPlay == true)
                ? MediaQuery.of(context).padding.bottom + 50
                : 0,
        color: HexColor('D4F1CF'),
        child: (audioProvider.storyModel != null)
            ? Row(
                children: [
                  Container(
                    margin:
                        EdgeInsets.only(left: 10, right: 3, top: 3, bottom: 3),
                    child: (audioProvider.storyModel.image != null)
                        ? Image.network(
                            ApiCons.domain + audioProvider.storyModel.image,
                            fit: BoxFit.fill,
                            errorBuilder: (BuildContext context,
                                Object exception, StackTrace stackTrace) {
                              return Image.asset(
                                  'assets/images/thumbnail_story.png');
                            },
                          )
                        : Image.asset('assets/images/thumbnail_story.png'),
                  )
                ],
              )
            : Container());
  }
}
