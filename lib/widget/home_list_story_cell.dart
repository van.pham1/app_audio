import 'package:app_audio/constants.dart';
import 'package:app_audio/widget/detail_story_page.dart';
import 'package:app_audio/widget/story_row.dart';
import 'package:flutter/material.dart';
import '../model/category_model.dart';

class HomeListStory extends StatefulWidget {
  final List<CategoryModel> listCate;
  final int section;

  HomeListStory({this.listCate, this.section});

  @override
  _HomeListStoryState createState() => _HomeListStoryState();
}

class _HomeListStoryState extends State<HomeListStory> {
  void goToView(int index) async {
    var data = await Navigator.push(context,
        new MaterialPageRoute(builder: (BuildContext context) {
      return new DetailStory(storyModel: widget.listCate[widget.section].categoryList[index],);
    }));

    print(data);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.listCate[widget.section].title,
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            child: ListView.separated(
                primary: false,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return FlatButton(
                    padding: const EdgeInsets.all(0),
                    onPressed: () {
                      Navigator.pushNamed(context, DetailStory.routeDetail,
                          arguments: widget.listCate[widget.section].categoryList[index]);
                      // goToView(index);
                    },
                    child: StoryRow(
                      index: index,
                      storyModel:
                          widget.listCate[widget.section].categoryList[index],
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    Container(),
                itemCount:( widget.listCate[widget.section].categoryList.length > 3) ? 3 : widget.listCate[widget.section].categoryList.length),
          ),
          SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: () {
              print('------ see more');
            },
            child: Container(
              margin: EdgeInsets.only(bottom: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Xem Thêm',
                    style: TextStyle(fontSize: 14, color: HexColor('#1E562A')),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
