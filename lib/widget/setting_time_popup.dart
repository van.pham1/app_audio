import 'package:flutter/material.dart';

class SettingTimePopup extends StatefulWidget {
  @override
  _SettingTimePopupState createState() => _SettingTimePopupState();
}

class _SettingTimePopupState extends State<SettingTimePopup> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        content: Container(
            height: 200,
            width: 150,
            child: FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('OK'))));
  }
}
