import 'dart:ui';

import 'package:app_audio/api_url.dart';
import 'package:app_audio/components/custom_loader.dart';
import 'package:app_audio/constants.dart';
import 'package:app_audio/helper/custom_slider.dart';
import 'package:app_audio/model/story_model.dart';
import 'package:app_audio/provider/audio_provider.dart';
import 'package:app_audio/widget/story_row.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import 'detail_info.dart';
import 'detail_story_row.dart';
import 'setting_time_popup.dart';

class DetailStory extends StatefulWidget {
  static const routeDetail = "DetailStory";
  final StoryModel storyModel;

  DetailStory({Key key, this.storyModel}) : super(key: key);

  @override
  _DetailStoryState createState() => _DetailStoryState();
}

class _DetailStoryState extends State<DetailStory> {
  CustomLoader loader = CustomLoader();

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      Provider.of<AudioProvider>(context, listen: false)
          .playAudio(widget.storyModel);
      loader.showLoader(context);
      Provider.of<AudioProvider>(context, listen: false)
          .getData(widget.storyModel.categoryType, 0)
          .then((value) => {loader.hideLoader()});
    });
  }

  Widget headerView() {
    return Consumer<AudioProvider>(builder:
        (BuildContext context, AudioProvider audioProvider, Widget child) {
      // print('===========123${audioProvider.storyModel.image}');
      return Container(
        decoration: BoxDecoration(
          border: Border.all(width: 1),
          color: Colors.white,
          image: DecorationImage(
            image: (audioProvider.storyModel.image != null)
                ? NetworkImage(ApiCons.domain + audioProvider.storyModel.image, )
                : AssetImage('assets/images/thumbnail_story.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: Stack(
          alignment: AlignmentDirectional.bottomEnd,
          children: [
            BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
              child: Container(
                color: Colors.black.withOpacity(0.3),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                  child: Container(
                    color: Colors.black.withOpacity(0.9),
                  ),
                ),
                Text(
                  audioProvider.storyModel.title,
                  style: TextStyle(color: HexColor('0F0F0F'), fontSize: 12),
                ),
                SizedBox(
                  height: 15,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  child: Container(
                    height: 90,
                    width: 90,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: (widget.storyModel.image != null)
                        ? Image.network(
                            ApiCons.domain + audioProvider.storyModel.image,
                            fit: BoxFit.fill,
                            errorBuilder: (BuildContext context,
                                Object exception, StackTrace stackTrace) {
                              return Image.asset(
                                  'assets/images/thumbnail_story.png');
                            },
                          )
                        : Image.asset(
                            'assets/images/thumbnail_story.png',
                            fit: BoxFit.cover,
                          ),
                  ),
                ),
                SliderWidget(
                  max: (audioProvider.duration != null)
                      ? audioProvider.duration.inSeconds
                      : 0,
                  audioProvider: audioProvider,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        audioProvider.playPrev();
                      },
                      child: Container(
                          height: 20,
                          width: 20,
                          child: Image.asset('assets/images/back_icon.png')),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        audioProvider.playOrPauseAudio();
                      },
                      child: Container(
                        height: 32,
                        width: 32,
                        child: Image.asset(audioProvider.isPlay
                            ? 'assets/images/pause_icon.png'
                            : 'assets/images/play_icon.png'),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        audioProvider.playNext();
                      },
                      child: Container(
                        height: 20,
                        width: 20,
                        child: Image.asset('assets/images/next_icon.png'),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
              ],
            ),
            Positioned(
              bottom: 22,
              right: 20,
              child: GestureDetector(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return SettingTimePopup();
                      });
                },
                child: Container(
                  height: 20,
                  width: 20,
                  child: Image.asset('assets/images/time_icon.png'),
                ),
              ),
            )
          ],
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (context, innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 290,
              floating: false,
              pinned: false,
              leading: Consumer<AudioProvider>(builder: (BuildContext context,
                  AudioProvider audioProvider, Widget child) {
                return IconButton(
                    icon: Icon(Icons.arrow_back, color: Color(0xFF2E2A2A)),
                    onPressed: () {
                      Navigator.of(context).pop();
                    });
              }),
              backgroundColor: Colors.white,
              flexibleSpace: FlexibleSpaceBar(
                background: headerView(),
              ),
            )
          ];
        },
        body: Container(
          margin: EdgeInsets.only(left: 20, right: 20),
          child: ListView(
            children: [
              DetailInfo(
                storyModel: widget.storyModel,
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                'Có thể bạn muốn nghe',
                style: TextStyle(
                    fontSize: 20,
                    color: Color(0xFF0F0F0F).withOpacity(0.86),
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 15,
              ),
              Consumer<AudioProvider>(builder: (BuildContext context,
                  AudioProvider audioProvider, Widget child) {
                return (audioProvider.storyList != null)
                    ? ListView.separated(
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        separatorBuilder: (BuildContext context, int index) =>
                            Divider(),
                        itemBuilder: (context, index) {
                          return DetailStoryRow(
                            storyModel: audioProvider.storyList[index],
                          );
                        },
                        itemCount: audioProvider.storyList.length)
                    : Container();
              }),
            ],
          ),
        ),
      ),
    );
  }
}
