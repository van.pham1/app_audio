
import 'package:app_audio/constants.dart';
import 'package:app_audio/model/story_model.dart';
import 'package:flutter/material.dart';

class DetailInfo extends StatefulWidget {
  final StoryModel storyModel;
  DetailInfo({this.storyModel});
  @override
  _DetailInfoState createState() => _DetailInfoState();
}

class _DetailInfoState extends State<DetailInfo> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                'Thông tin chi tiết',
                style: TextStyle(
                    color: Color(0xFF0F0F0F).withOpacity(0.86),
                    fontWeight: FontWeight.bold,
                    fontSize: 17),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                '(Thành viên VIP)',
                style: TextStyle(
                    color: Color(0xFFD9A70E),
                    fontWeight: FontWeight.bold,
                    fontSize: 17),
              ),
              SizedBox(
                width: 5,
              ),
              Container(
                height: 30,
                width: 30,
                child: Image.asset('assets/images/icon_vip.png'),
              )
            ],
          ),
          Divider(height: 18,color: HexColor('D4F1CF'),),
          SizedBox(
            height: 5,
          ),
          Text('Thể loại: Truyện ngụ ngôn'),
          Divider(height: 18,color: HexColor('D4F1CF'),),
          Text(
              'Ngày cập nhật: ${widget.storyModel.createdAt.substring(0, 10)}'),
          Divider(height: 18,color: HexColor('D4F1CF'),),
          Text('Thời gian: ${convertToTime()}'),
          Divider(height: 18,color: HexColor('D4F1CF'),),
          Text('Lượt nghe: ${widget.storyModel.viewTotal}'),
          Divider(height: 18,color: HexColor('D4F1CF'),),
        ],
      ),
    );
  }

  String convertToTime() {
    String timeStr;
    final time = widget.storyModel.timeTotal;
    final second = time % 60;
    final minutes = (time ~/ 60) % 60;
    print('$minutes');
    print('$second');
    if (second < 10) {
      if (minutes < 10) {
        timeStr = '0$minutes:0$second';
      } else {
        timeStr = '$minutes:0$second';
      }
    } else {
      if (minutes < 10) {
        timeStr = '0$minutes:$second';
      } else {
        timeStr = '$minutes:$second';
      }
    }
    return timeStr;
  }
}
