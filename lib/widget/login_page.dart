import 'package:app_audio/provider/login_provider.dart';
import 'package:flutter/material.dart';
import 'dart:io' show Platform;
import 'package:app_audio/constants.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  final LoginProvider loginProvider;
  LoginPage({this.loginProvider});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    // throw UnimplementedError();
    return Column(
      children: [
        Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(0, 70.3, 0, 0),
            child: Stack(
              children: [
                Image.asset(
                  'assets/images/logo_backg.png',
                  width: double.infinity,
                  fit: BoxFit.fitWidth,
                  alignment: Alignment.center,
                ),
                Image.asset(
                  'assets/images/logo.png',
                  width: double.infinity,
                  alignment: Alignment.center,
                ),
              ],
            )),
        Spacer(),
        Container(
          child: Column(
            children: <Widget>[
              Container(
                  // margin: EdgeInsets.only(top: 14),
                  child: Text('Truyện Cổ Tích Audio',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 24, fontFamily: 'Pacifico'))),
              LoginApple(),
              LoginFacebook(),
              LoginGoogle(),
              Container(
                margin: EdgeInsets.only(top: 0, bottom: 10),
                child: SafeArea(
                  child: RichText(
                    text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          text: "Điều khoản ",
                          style: TextStyle(color: Colors.deepOrange)),
                      TextSpan(
                          text: "người dùng và ",
                          style: TextStyle(color: Colors.black)),
                      TextSpan(
                          text: "Bảo mật ",
                          style: TextStyle(color: Colors.deepOrange)),
                      TextSpan(
                          text: "thông tin ",
                          style: TextStyle(color: Colors.black)),
                    ]),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class LoginApple extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      return Container(
        margin: EdgeInsets.fromLTRB(20, 9, 20, 5),
        child: FlatButton.icon(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(22.5),
          ),
          onPressed: () {},
          minWidth: double.infinity,
          height: 45,
          icon: Image.asset('assets/images/icon_apple.png'),
          label: Text('Login with Apple'),
          color: HexColor("#000000"),
          textColor: HexColor("#FFFFFF"),
        ),
      );
    } else {
      return Container();
    }
  }
}

class LoginGoogle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 0, 20, 5),
      child: FlatButton(
        onPressed: () {
        },
        minWidth: double.infinity,
        height: 45,
        child: Text('Google'),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(22.5),
            side: BorderSide(color: HexColor("#1E562A"))),
      ),
    );
  }
}

class LoginFacebook extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final loginProvider = Provider.of<LoginProvider>(context);
    return Container(
      margin: EdgeInsets.fromLTRB(20, 0, 20, 5),
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(22.5),
            topRight: Radius.circular(22.5),
            bottomLeft: Radius.circular(22.5),
            bottomRight: Radius.circular(22.5)),
        boxShadow: [
          BoxShadow(
            color: HexColor("#1E562A").withOpacity(0.2),
            spreadRadius: 1,
            blurRadius: 3,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: FlatButton(
        onPressed: () {
          loginProvider.loginFb().then((value) => {print('done')});
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(22.5),
        ),
        minWidth: double.infinity,
        height: 45,
        child: Text('Facebook'),
        color: HexColor("#1E562A"),
        textColor: HexColor("#ffffff"),
      ),
    );
  }
}

