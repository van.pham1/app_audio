import 'package:app_audio/api_url.dart';
import 'package:app_audio/model/story_model.dart';
import 'package:app_audio/provider/audio_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailStoryRow extends StatelessWidget {
  final StoryModel storyModel;

  DetailStoryRow({this.storyModel});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {

      },
      child: Container(
        margin: EdgeInsets.only(left: 10, right: 10),
        child: Row(
          children: [
            SizedBox(
              height: 10,
            ),
            ClipRRect(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
              child: ClipRRect(
                child: Container(
                  height: 60,
                  width: 100,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: (storyModel.image != null)
                      ? Image.network(
                          ApiCons.domain + '${storyModel.image}',
                          fit: BoxFit.cover,
                          errorBuilder: (BuildContext context, Object exception,
                              StackTrace stackTrace) {
                            return Image.asset(
                                'assets/images/thumbnail_story.png');
                          },
                        )
                      : Image.asset(
                          'assets/images/thumbnail_story.png',
                        ),
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          '${storyModel.title}',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            height: 20,
                            width: 20,
                            child: storyModel.statusVip == 1
                                ? Image.asset('assets/images/ic_VIP.png')
                                : Container(),
                          )
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Text('Thể loại: Truyện cổ tích'),
                  SizedBox(height: 5),
                  Text('Lượt nghe: ${storyModel.viewTotal}'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
