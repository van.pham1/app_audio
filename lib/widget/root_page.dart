import 'package:app_audio/provider/login_provider.dart';
import '../model/user.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import './login_page.dart';
import './home_page.dart';
import 'package:flutter/material.dart';

class RootPage extends StatefulWidget {
  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  Widget root;

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback(
          (_) => Provider.of<LoginProvider>(context, listen: false).loadUserLogin(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LoginProvider>(builder:
        (BuildContext context, LoginProvider loginProvider, Widget child) {
      return Scaffold(body: loadRoot(loginProvider));
    });
  }

  Widget loadRoot(LoginProvider loginProvider) {
    User loginUser = loginProvider.userLogin;
    print(loginUser == null);
    return loginProvider.userLogin != null
        ? HomePage()
        : LoginPage(loginProvider: loginProvider);
  }
}
