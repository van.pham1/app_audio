import 'package:app_audio/constants.dart';
import 'package:flutter/material.dart';

class CategoryCell extends StatelessWidget {
  final String listCate;
  CategoryCell({this.listCate});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 20,right: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 6),
            color: HexColor('#D4F1CF'),
            child: Row(children: [
              Container(
                width: 1,
                height: 30,
                color: HexColor('#1E562A'),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                '${listCate}',
                style: TextStyle(fontSize: 16, color: HexColor('#1E562A'), fontWeight: FontWeight.w500),
              ),
              Spacer(),
              Icon(Icons.chevron_right_sharp)
            ]),
            height: 30,
          )
        ],
      ),
    );
  }
}