import 'package:app_audio/api_url.dart';
import 'package:app_audio/constants.dart';
import 'package:app_audio/model/story_model.dart';
import 'package:flutter/material.dart';

class StoryRow extends StatefulWidget {
  final StoryModel storyModel;
  final int index;

  StoryRow({this.index, this.storyModel});

  @override
  _StoryRowState createState() => _StoryRowState();
}

class _StoryRowState extends State<StoryRow> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 5),
          height: 56,
          child: Row(
              children: [
                Text(
                  '0${widget.index + 1}',
                  style: TextStyle(
                      color: widget.index < 3
                          ? HexColor('#BF8877')
                          : HexColor('#B5BEB7'),
                      decoration: TextDecoration.underline,
                      fontWeight: FontWeight.bold,
                      wordSpacing: 3.0),
                ),
                SizedBox(
                  width: 5,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                    child: Container(
                      height: 56,
                      width: 56,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: widget.storyModel.image != null ? Image.network(ApiCons.domain +
                        '${widget.storyModel.image}',
                        fit: BoxFit.fitWidth,
                        errorBuilder: (BuildContext context, Object exception,
                            StackTrace stackTrace) {
                          return Image.asset(
                              'assets/images/thumbnail_story.png');
                        },
                      ) : Image.asset('assets/images/thumbnail_story.png'),
                    ),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '${widget.storyModel.title}',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 14),
                      ),
                      SizedBox(
                        width: 5,
                        height: 4,
                      ),
                      Text(
                        'Thể loại : ${widget.storyModel.storyCateName}',
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: TextStyle(fontSize: 10),
                      ),
                      SizedBox(
                        width: 5,
                        height: 4,
                      ),
                      Text(
                        'Lượt nghe : ${widget.storyModel.viewTotal}',
                        maxLines: 1,
                        style: TextStyle(fontSize: 10),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      height: 24,
                      width: 24,
                      child: widget.storyModel.statusVip == 1
                          ? Image.asset('assets/images/ic_VIP.png')
                          : Container(),
                    )
                  ],
                ),
              ],
            ),
          ),
         Divider(),
      ],
    );
  }
}
