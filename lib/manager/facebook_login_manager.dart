import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class FacebookSigninManager {
  Future<FacebookLoginResult> signin() async {
    final facebookLogin = FacebookLogin();
    final result = await facebookLogin.logIn(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        print('Login success');
        break;
      case FacebookLoginStatus.cancelledByUser:
        print('User cancel');
        break;
      case FacebookLoginStatus.error:
        print('Login fail');
        break;
    }
    return result;
  }
}
