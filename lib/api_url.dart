class ApiCons {
  static String baseURL = "https://truyencotichaudio.com/api/";
  static String domain = "https://truyencotichaudio.com";
  static String getListStoryUp = "getListStoryHomeUp";
  static String storyCat = "getListStoryHome";
  static String login = "auth/login";
}

class APIParams {
  static String googleId = "google_id";
  static String fbId = "provider_id";
  static String appleId = "apple_id";
  static String name = "name";
  static String avatar = "avatar";
  static String email = "email";
  static String version = "version";
  static String communication = "communication";
  static String type_login = "type_login";
  static String id = "id";
  static String url = "url";
  static String facebook = "facebook";
  static String google = 'google';
  static String typeList = "type_list";
  static String page = "page";
  static String limit = "limit";
  static String pageCategory = "page_category";
  static String limitCategory = "limit_category";
  static String categoryType = "category_type";
}