import 'dart:convert';

import 'package:app_audio/api/api_webservice.dart';
import 'package:app_audio/api/app_request_status.dart';
import 'package:app_audio/api_url.dart';
import 'package:app_audio/model/list_category.dart';
import 'package:app_audio/model/story_model.dart';
import 'package:app_audio/provider/home_provider.dart';
import 'package:audioplayers/audioplayers.dart';

class AudioProvider extends AppNotifier {
  AudioPlayer _audioPlayer = AudioPlayer();
  bool isPlay;
  Duration duration;
  Duration position;
  String durationTxt;
  String positionTxt;
  List<StoryModel> storyList;
  StoryModel storyModel;
  int _index = 0;
  ApiWebservice apiWebservice = ApiWebservice();

  void playAudio(StoryModel story) async {
    if (storyList != null && story.id == storyModel.id) {
      return;
    }
    storyModel = story;
    await _audioPlayer.stop();
    isPlay = true;
    // notifyListeners();
    await _audioPlayer.play(Uri.encodeFull((ApiCons.domain + story.path)));
    notifyListeners();
    _audioPlayer.onDurationChanged.listen((Duration d) {
      duration = d;
      durationTxt = (duration?.toString()?.split('.')?.first ?? '').substring(2);
      notifyListeners();
    });
    _audioPlayer.onAudioPositionChanged.listen((Duration d) {
      position = d;
      positionTxt = (position?.toString()?.split('.')?.first ?? '').substring(2);
      notifyListeners();
    });
  }

  void playPrev() async {
    if (_index > 1) {
      _index -= 1;
      playAudio(storyList[_index]);
    }
  }

  void playNext() async {
    if (_index < storyList.length - 2) {
      _index += 1;
      playAudio(storyList[_index]);
    }
  }

  Future<void> getData(String cateType, int page) async {
    setApiRequestStatus(APIRequestStatus.loading);
    try {
      List<dynamic> listCate = json.decode(cateType);
      ListCategory list = await apiWebservice.getListSimilarStory(
          int.parse(listCate.first), page);
      convertToListStory(list);
      setApiRequestStatus(APIRequestStatus.loaded);
    } catch (e) {
      checkError(e);
    }
  }

  convertToListStory(ListCategory listStory) {
    storyList = List<StoryModel>();
    for (var cate in listStory.data) {
      for (var story in cate.categoryList) {
        print('=======aaaa${story.image}');
        storyList.add(story);
      }
    }
    notifyListeners();
  }

  void seekToPosition(double position) {
    _audioPlayer.seek(Duration(seconds: position.toInt()));
  }

  Future<void> playOrPauseAudio() async {
    isPlay = !isPlay;
    notifyListeners();
    print('---------playOrPauseAudio $isPlay');
    if (isPlay) {
      final result = await _audioPlayer.resume();
      print('---------resume $result');
    } else {
      final result = await _audioPlayer.pause();
      print('---------pause $result');
    }
  }

  stopAudio() async {
    await _audioPlayer.stop();
  }

}