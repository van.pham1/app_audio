import 'package:app_audio/constants.dart';
import 'package:app_audio/model/category_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:app_audio/api/api_webservice.dart';
import 'package:app_audio/model/list_category.dart';

import '../api/app_request_status.dart';
import '../api/error_handle.dart';
import 'package:flutter/material.dart';
class HomeProvider extends AppNotifier {
  ListCategory listCategory;
  ListCategory listStory;
  APIRequestStatus apiRequestStatus = APIRequestStatus.loading;
  ApiWebservice apiWebservice = ApiWebservice();
  Future<void> getData() async {
    setApiRequestStatus(APIRequestStatus.loading);
    try {
      ListCategory listStory = await apiWebservice.getListStory();
      setListStory(listStory);
      listCategory = await apiWebservice.getListCate();
      setListCategory(listCategory);
    } catch (e) {
      checkError(e);
    }
  }


  void setListCategory(ListCategory listCategory) {
    AppConstant.shared.listCate = listCategory;
    listCategory = listCategory;
    notifyListeners();
  }

  void setListStory(ListCategory listCategory) {
    listStory = listCategory;
    notifyListeners();
  }

}

class AppNotifier extends ChangeNotifier {
  APIRequestStatus apiRequestStatus = APIRequestStatus.loading;

  void checkError(e) {
    if (ErrorHandle.checkConnectionError(e)) {
      setApiRequestStatus(APIRequestStatus.connectionError);
    } else {
      setApiRequestStatus(APIRequestStatus.error);
    }
  }

  void setApiRequestStatus(APIRequestStatus value) {
    apiRequestStatus = value;
    notifyListeners();
  }
}