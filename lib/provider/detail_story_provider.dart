import 'dart:convert';

import 'package:app_audio/api/api_webservice.dart';
import 'package:app_audio/api/app_request_status.dart';
import 'package:app_audio/api/error_handle.dart';
import 'package:app_audio/model/list_category.dart';
import 'package:app_audio/model/story_model.dart';
import 'package:flutter/material.dart';

class DetailStoryProvider extends ChangeNotifier {
  List<StoryModel> storyList;
  ApiWebservice apiWebservice = ApiWebservice();
  APIRequestStatus apiRequestStatus = APIRequestStatus.loading;
  Future<void> getData(String cateType, int page) async {
    try {
      List<dynamic> listCate = json.decode(cateType);
      ListCategory list = await apiWebservice.getListSimilarStory(
          int.parse(listCate.first), page);
      convertToListStory(list);
    } catch (e) {
      checkError(e);
    }
  }
  convertToListStory(ListCategory listStory) {
    storyList = List<StoryModel>();
    print('---------convertToListStory 1 ${listStory.data.length}');
    for (var cate in listStory.data) {
      print('---------convertToListStory 2 ${cate.categoryList.length}');
      for (var story in cate.categoryList) {
        storyList.add(story);
        print('---------convertToListStory 4 ${storyList.length}');
      }
    }
    print('---------convertToListStory 3 ${storyList.length}');
    notifyListeners();
  }

  void checkError(e) {
    if (ErrorHandle.checkConnectionError(e)) {
      setApiRequestStatus(APIRequestStatus.connectionError);
    } else {
      setApiRequestStatus(APIRequestStatus.error);
    }
  }

  void setApiRequestStatus(APIRequestStatus value) {
    apiRequestStatus = value;
    notifyListeners();
  }
}