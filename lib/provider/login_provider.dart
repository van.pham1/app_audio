import 'package:app_audio/api/api_webservice.dart';
import 'package:app_audio/api/app_request_status.dart';
import 'package:app_audio/manager/shared_preference.dart';
import 'package:app_audio/model/user.dart';
import 'package:app_audio/provider/home_provider.dart';

import '../constants.dart';

class LoginProvider extends AppNotifier {
  User userLogin;
  APIRequestStatus apiRequestStatus = APIRequestStatus.loading;
  ApiWebservice apiWebserver = ApiWebservice();

  Future<void> loginFb() async {
    setApiRequestStatus(APIRequestStatus.loading);
    print('api');
    try {
      User user = await apiWebserver.signInFacebook();
      setUserLogin(user);
      setApiRequestStatus(APIRequestStatus.loaded);
    } catch (e) {
      print(e);
      checkError(e);
    }
  }

  // Future<void> loginGoogle() async {
  //   setApiRequestStatus(APIRequestStatus.loading);
  //   try {
  //     User user = await apiWebserver.signInGoogle();
  //     setUserLogin(user);
  //     setApiRequestStatus(APIRequestStatus.loaded);
  //   } catch (e) {
  //     print("LoginProvider: loginFb error: ");
  //     print(e);
  //     checkError(e);
  //   }
  // }

  void setUserLogin(User user) {
    userLogin = user;
    SharedPreference.shared.save(SharedPrefCons.userLogin, user);
    notifyListeners();
  }

  Future<void> loadUserLogin() async {
    print("Load user Login ");
    User user;
    try {
      user = User.fromJson(
          await SharedPreference.shared.read(SharedPrefCons.userLogin));
      print("loadUserLogin success");
    } catch (e) {
      print("loadUserLogin fail");
      print(e);
    }
    setUserLogin(user);
  }
}