import 'package:app_audio/model/story_model.dart';
import 'package:app_audio/widget/detail_story_page.dart';
import 'package:flutter/material.dart';

import 'custom_route.dart';

class Routes {
  static Route onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case DetailStory.routeDetail:
        final StoryModel storyModel = settings.arguments;
        return CustomRoute<bool>(
            builder: (BuildContext context) => DetailStory(
              storyModel: storyModel,
            ));
      default:
        return onUnknownRoute(RouteSettings(name: '/Feature'));
    }
  }

  static Route onUnknownRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (_) => Scaffold(
          appBar: AppBar(title: Text('Not Found Page')),
          body: Center(
            child: Text('${settings.name.split('/')[1]} Comming soon..'),
          )),
    );
  }
}
