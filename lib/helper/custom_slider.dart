
import 'package:app_audio/provider/audio_provider.dart';
import 'package:flutter/material.dart';

class SliderWidget extends StatefulWidget {
  final double sliderHeight;
  final int min;
  final int max;
  final fullWidth;
  final AudioProvider audioProvider;

  SliderWidget(
      {this.sliderHeight = 48,
        this.max = 10,
        this.min = 0,
        this.fullWidth = false,
        this.audioProvider});

  @override
  _SliderWidgetState createState() => _SliderWidgetState();
}

class _SliderWidgetState extends State<SliderWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(
        left: 30,
        right: 30,
      ),
      height: (this.widget.sliderHeight),
      decoration: new BoxDecoration(
        borderRadius: new BorderRadius.all(
          Radius.circular((this.widget.sliderHeight * .3)),
        ),
      ),
      child: Container(
        child: SliderTheme(
          data: SliderTheme.of(context).copyWith(
            activeTrackColor: Color(0xFF1E562A),
            inactiveTrackColor: Color(0xFFB5BEB7),
            trackShape: CustomTrackShape(),
            trackHeight: 2.0,
            thumbShape: CustomSliderThumbRect(
                thumbRadius: 20,
                min: this.widget.min,
                max: this.widget.max,
                thumbHeight: 40,
                durationTxt: widget.audioProvider.durationTxt,
                positionTxt: widget.audioProvider.positionTxt),
            overlayColor: Colors.white.withOpacity(.4),
            //valueIndicatorColor: Colors.white,
            activeTickMarkColor: Colors.white,
            inactiveTickMarkColor: Colors.red.withOpacity(.7),
          ),
          child: Slider(
              min: 0.0,
              max: widget.max.toDouble(),
              value: (widget.audioProvider.position != null)
                  ? widget.audioProvider.position.inSeconds.toDouble()
                  : 0,
              onChanged: (value) {
                setState(() {
                  widget.audioProvider.seekToPosition(value);
                });
              }),
        ),
      ),
    );
  }
}

class CustomTrackShape extends RoundedRectSliderTrackShape {
  Rect getPreferredRect({
    @required RenderBox parentBox,
    Offset offset = Offset.zero,
    @required SliderThemeData sliderTheme,
    bool isEnabled = false,
    bool isDiscrete = false,
  }) {
    final double trackHeight = sliderTheme.trackHeight;
    final double trackLeft = offset.dx;
    final double trackTop =
        offset.dy + (parentBox.size.height - trackHeight) / 2;
    final double trackWidth = parentBox.size.width;
    return Rect.fromLTWH(trackLeft, trackTop, trackWidth, trackHeight);
  }
}

class CustomSliderThumbRect extends SliderComponentShape {
  final double thumbRadius;
  final thumbHeight;
  final int min;
  final int max;
  final String durationTxt;
  final String positionTxt;

  const CustomSliderThumbRect(
      {this.thumbRadius,
        this.thumbHeight,
        this.min,
        this.max,
        this.durationTxt,
        this.positionTxt});

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size.fromRadius(thumbRadius);
  }

  @override
  void paint(
      PaintingContext context,
      Offset center, {
        Animation<double> activationAnimation,
        Animation<double> enableAnimation,
        bool isDiscrete,
        TextPainter labelPainter,
        RenderBox parentBox,
        SliderThemeData sliderTheme,
        TextDirection textDirection,
        double value,
        double textScaleFactor,
        Size sizeWithOverflow,
      }) {
    final Canvas canvas = context.canvas;

    final paint = Paint()
      ..color = Color(0xFF1E562A) //Thumb Background Color
      ..style = PaintingStyle.fill;

    TextSpan span = new TextSpan(
        style: new TextStyle(
            fontSize: 10,
            fontWeight: FontWeight.w700,
            color: Colors.white,
            height: 1),
        text: '$positionTxt/$durationTxt');
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    Offset textCenter =
    Offset(center.dx - (tp.width / 2), center.dy - (tp.height / 2));

    final rRect = RRect.fromRectAndRadius(
      Rect.fromCenter(
          center: center,
          width: tp.size.width + 5,
          height: tp.size.height + 10),
      Radius.circular(thumbRadius * .4),
    );
    canvas.drawRRect(rRect, paint);
    tp.paint(canvas, textCenter);
  }

  String getValue(double value) {
    return (min + (max - min) * value).round().toString();
  }
}
