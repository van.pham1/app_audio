import 'package:app_audio/provider/audio_provider.dart';
import 'package:app_audio/provider/detail_story_provider.dart';
import 'package:app_audio/provider/home_provider.dart';
import 'package:app_audio/provider/login_provider.dart';
import 'package:app_audio/widget/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'helper/routes.dart';

void main() => runApp(MultiProvider(
      child: MyApp(),
      providers: [
        ChangeNotifierProvider(create: (_) => HomeProvider()),
        ChangeNotifierProvider(create: (_) => LoginProvider()),
        ChangeNotifierProvider(create: (_) => AudioProvider()),
        ChangeNotifierProvider(create: (_) => DetailStoryProvider()),
      ],
    ));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Scaffold(
          body: SplashScreen(), //   <--- image
        ),
        onGenerateRoute: (settings) => Routes.onGenerateRoute(settings));
  }
}
