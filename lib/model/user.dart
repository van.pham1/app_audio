import '../model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';
part 'user.g.dart';

@JsonSerializable()
class User extends BaseModel {
  final int id;

  @JsonKey(name: 'password_default')
  final String passwordDefault;

  @JsonKey(name: 'avatar_original')
  final String avatarOriginal;

  @JsonKey(name: 'created_at')
  final String createdAt;

  final String email;

  @JsonKey(name: 'updated_at')
  final String updatedAt;

  final String token;

  final String name;

  final int status;

  User(
      {this.id,
        this.passwordDefault,
        this.avatarOriginal,
        this.createdAt,
        this.email,
        this.updatedAt,
        this.token,
        this.name,
        this.status});
  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}