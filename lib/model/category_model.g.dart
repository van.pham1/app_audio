// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryModel _$CategoryModelFromJson(Map<String, dynamic> json) {
  return CategoryModel(
    status: json['status'] as int,
    id: json['id'] as int,
    title: json['title'] as String,
    createdAt: json['created_at'] as String,
    updatedAt: json['updated_at'] as String,
    categoryList: (json['category_list'] as List)
        ?.map((e) =>
            e == null ? null : StoryModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CategoryModelToJson(CategoryModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'id': instance.id,
      'title': instance.title,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
      'category_list': instance.categoryList,
    };
