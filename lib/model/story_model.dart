import 'package:app_audio/constants.dart';
import 'package:app_audio/model/category_model.dart';
import 'package:app_audio/model/list_category.dart';
import 'package:app_audio/provider/home_provider.dart';
import 'package:json_annotation/json_annotation.dart';
import 'dart:convert' show utf8;

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

part 'story_model.g.dart';


@JsonSerializable()
class StoryModel {
  int id;

  String title;

  @JsonKey(name: 'category_type')
  String categoryType;

  @JsonKey(name: 'view_total')
  int viewTotal;

  String path;

  String image;

  @JsonKey(name: 'time_total')
  int timeTotal;

  @JsonKey(name: 'created_at')
  String createdAt;

  @JsonKey(name: 'updated_at')
  String updatedAt;

  @JsonKey(name: 'status_vip')
  int statusVip;

  String detail;

  String _cateName;


  String get storyCateName {
    if (_cateName != null) {
      return _cateName;
    } else {
      return setCategoryName();
    }
  }

  StoryModel(
      {this.id,
        this.title,
        this.categoryType,
        this.viewTotal,
        this.path,
        this.image,
        this.timeTotal,
        this.createdAt,
        this.updatedAt,
        this.statusVip,
        this.detail});

  factory StoryModel.fromJson(Map<String, dynamic> json) =>
      _$StoryModelFromJson(json);
  Map<String, dynamic> toJson() => _$StoryModelToJson(this);

  List<int> getCategories() {
    var arr = <int>[];
    var list = List<String>();
    final str = categoryType;
    final data = utf8.encode(str);
    final String decoded = utf8.decode(data);
    var lists = json.decode(decoded) as List;
    list = List.from(lists);
    arr = list.map(int.parse).toList();
    print(list);
    return arr;
  }

  String setCategoryName() {
    if (AppConstant.shared.listCate != null) {
       ListCategory _listCategory = AppConstant.shared.listCate;
       print(_listCategory.data.length);
       var arr = getCategories();
      for (CategoryModel cat in _listCategory.data) {
        for (var i in arr) {
          print(cat.title);
          if (cat.id == i) {
            print('===${cat.title}');
            return cat.title;
          }
        }
      }
    }
    // print(cateArr);

    // var title = catelists.join(', ');
    // _cateName = title;
    // return title;
  }

}