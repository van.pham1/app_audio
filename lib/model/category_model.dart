import 'story_model.dart';
import 'package:json_annotation/json_annotation.dart';
part 'category_model.g.dart';

@JsonSerializable()
class CategoryModel {
  int status;

  int id;

  String title;

  @JsonKey(name: 'created_at')
  String createdAt;

  @JsonKey(name: 'updated_at')
  String updatedAt;

  @JsonKey(name: 'category_list')
  List<StoryModel> categoryList;

  CategoryModel(
      {this.status,
        this.id,
        this.title,
        this.createdAt,
        this.updatedAt,
        this.categoryList});
  factory CategoryModel.fromJson(Map<String, dynamic> json) =>
      _$CategoryModelFromJson(json);
  Map<String, dynamic> toJson() => _$CategoryModelToJson(this);
}