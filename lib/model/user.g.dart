// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['id'] as int,
    passwordDefault: json['password_default'] as String,
    avatarOriginal: json['avatar_original'] as String,
    createdAt: json['created_at'] as String,
    email: json['email'] as String,
    updatedAt: json['updated_at'] as String,
    token: json['token'] as String,
    name: json['name'] as String,
    status: json['status'] as int,
  )..message = json['message'] as String;
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'message': instance.message,
      'id': instance.id,
      'password_default': instance.passwordDefault,
      'avatar_original': instance.avatarOriginal,
      'created_at': instance.createdAt,
      'email': instance.email,
      'updated_at': instance.updatedAt,
      'token': instance.token,
      'name': instance.name,
      'status': instance.status,
    };
