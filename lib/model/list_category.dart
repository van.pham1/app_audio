import 'package:json_annotation/json_annotation.dart';
import 'category_model.dart';
import 'base_model.dart';
part 'list_category.g.dart';

@JsonSerializable()
class ListCategory extends BaseModel {
  List<CategoryModel> data;

  ListCategory({this.data});

  factory ListCategory.fromJson(Map<String, dynamic> json) =>
      _$ListCategoryFromJson(json);
  Map<String, dynamic> toJson() => _$ListCategoryToJson(this);

}